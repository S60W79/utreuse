
import QtQuick 2.9
import Ubuntu.Components 1.3

Page {
    property var list : root.relevo
	id:revDetails
    header: PageHeader {
        id:infoHeader
        title: i18n.tr("Details")
    }

    ListModel {
       id: infoModel
     }

    Component.onCompleted: {
        if(list["canBeExtended"]){
            //The item is now be able to extend.
            extendState.text = i18n.tr("You can extend the rental now.")
        }else{
            if(list["isExtended"]){
                //Has already been isExtended
                extendState.text = i18n.tr("The rental has already been extended, no furtther extension is possible.")
            }else{
                extendState.text = i18n.tr("You will later be able to extend the rental.")
            }
        }
        
    }

    Column {
        id: aboutCloumn
        spacing:units.dp(2)
        width:parent.width

        Label { //An hack to add margin to the column top
            width:parent.width
            height:infoHeader.height *2
        }

        Icon {
          anchors.horizontalCenter: parent.horizontalCenter
          height: Math.min(parent.width/2, parent.height/2)
          width:height
          source:list["product"]["image"]
          layer.enabled: true
          layer.effect: UbuntuShapeOverlay {
              relativeRadius: 0.75
           }
        }
        Label {
            width: parent.width
            font.pixelSize: units.gu(5)
            font.bold: true
            color: theme.palette.normal.backgroundText
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("Relevo ")+list["product"]["productName"]
        }
        Label {
            font.pixelSize: units.gu(2)
            width: parent.width
            color: theme.palette.normal.backgroundTertiaryText
            horizontalAlignment: Text.AlignHCenter
            text: i18n.tr("QR-id: ")+list["product"]["uId"]
        }
        Label {
            font.pixelSize: units.gu(2)
            width: parent.width
            text: i18n.tr("\nPlace of rental: ")+list["restaurant"]["restaurantName"]
        }
        Label {
            id:extendState
            font.pixelSize: units.gu(2)
            width: parent.width
            text:""
        }
       Button{
            width:parent.width
            text:i18n.tr("Transfer Item")
            iconName:"share"
            color: UbuntuColors.green
            onClicked:{
                root.qcode = list["product"]["uId"]
                pendingList.pageStack.addPageToNextColumn(pendingList, Qt.resolvedUrl("share.qml"))
            }
        }
        

    }


}

/*
 * Copyright (C) 2021  S60W79
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

