function logOutRe(id, authkey, lang, callback){
    //Log out of relevo
    console.log("Check","id", id, "auth", authkey);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.relevo-admin.de/api/v2/logoutUser");
    xhr.setRequestHeader("Authorization", "Bearer "+authkey);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    
    xhr.onreadystatechange = function () {
           console.log("LOG OUT", xhr.status, xhr.responseText);    
            if(xhr.status == 201){
                callback(true, JSON.parse(xhr.responseText));
            }else{
                callback(false, JSON.parse(xhr.responseText));
    }
    };

    var data = `{"user_id": "`+id+`"}`;
    xhr.send(data); 
} 
function sepaRe(id, authkey, lang, iban, fName, lName, callback){
    //connect Relevo Account width Sepa/Iban
    console.log("SEPA","id", id, "auth", authkey);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.relevo-admin.de/api/v2/users/"+id+"/payments/sepa");
    xhr.setRequestHeader("Authorization", "Bearer "+authkey);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    
    xhr.onreadystatechange = function () {
           console.log("SEPA", xhr.status, xhr.responseText);    
            if(xhr.status == 201){
                callback(true);
            }else{
                callback(false);
    }
    };

    var data = `{"firstName": "`+fName+`",
        "lastName": "`+lName+`",
        "iban": "`+iban+`"
    }`;
    xhr.send(data); 
} 
function infoRe(id, authkey, lang, callback){
    //connect Relevo Account width Sepa/Iban
    console.log("Check","id", id, "auth", authkey);
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api.relevo-admin.de/api/v2/users/"+id+"/user-info");
    xhr.setRequestHeader("Authorization", "Bearer "+authkey);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    
    xhr.onreadystatechange = function () {
           console.log("INFO", xhr.status, xhr.responseText);    
            if(xhr.status == 200){
                callback(true, JSON.parse(xhr.responseText));
            }else{
                callback(false, JSON.parse(xhr.responseText));
    }
    };

    xhr.send(); 
} 
