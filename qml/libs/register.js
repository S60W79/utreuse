function relevoReg(mail, pwd, dev, lang, callback){
    
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.relevo-admin.de/api/v2/register");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    
    xhr.onreadystatechange = function () {
           console.log("rental", xhr.status, xhr.responseText);    
            if(xhr.status == 201){
                callback(true, JSON.parse(xhr.responseText));
            }else{
                callback(false, JSON.parse(xhr.responseText));
    }
    };

    var data = `{
    "deviceToken": "`+dev+`",
    "deviceType": "1",
    "email": "`+mail+`",
    "password":"`+pwd+`"
    }`;
    xhr.send(data); 
} 
function relevoLog(mail, pwd, dev, lang, callback){
    
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.relevo-admin.de/api/v2/login");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    
    xhr.onreadystatechange = function () {
           console.log("LOGIN", xhr.status, xhr.responseText);    
            if(xhr.status == 200){
                callback(true, JSON.parse(xhr.responseText));
            }else{
                callback(false, JSON.parse(xhr.responseText));
    }
    };

    var data = `{
    "deviceToken": "`+dev+`",
    "deviceType": "1",
    "email": "`+mail+`",
    "password":"`+pwd+`"
    }`;
    xhr.send(data); 
} 
