function items(authkey, lang, callback){
    
    var reacted = false;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api.relevo-admin.de/api/v2/products?status=active&page=1&metaCategoryId=1");
    xhr.setRequestHeader("Authorization", "Bearer "+authkey);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    xhr.onreadystatechange = function () {
           console.log("LIST", xhr.status, xhr.responseText);    
            if(xhr.status == 200){
                callback(true, JSON.parse(xhr.responseText));
            }else{
                callback(false, JSON.parse(xhr.responseText));
    }
    };

    xhr.send(); 
} 
function extend(authkey, lang, id, callback){
    //extend items
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.relevo-admin.de/api/v2/products/"+id+"/extend");
    xhr.setRequestHeader("Authorization", "Bearer "+authkey);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    
    xhr.onreadystatechange = function () {
           console.log("INFO", xhr.status, xhr.responseText);    
            if(xhr.status == 200){
                callback(true, JSON.parse(xhr.responseText));
            }else{
                callback(false, JSON.parse(xhr.responseText));
    }
    };

    xhr.send(); 
}
