function scan(authkey, id, code, lang, callback){
    //if the id is an url, then replace / width %2F
    if(code.includes("https:")){
        code = code.replace("https:", "")
        code = encodeURIComponent(code);
        code = "https:"+code
        console.log("encoded", code);
    }
    //connect Relevo Account width Sepa/Iban
    console.log("Check","id", id, "auth", authkey);
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://api.relevo-admin.de/api/v2/products/scannedItemV2/"+id+"/"+code);
    xhr.setRequestHeader("Authorization", "Bearer "+authkey);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept-Encoding", "gzip, deflate");
    xhr.setRequestHeader("User-Agent", "Relevo/2.2.89 (Android 9)");
    xhr.setRequestHeader("X-Localization", lang);
    
    
    xhr.onreadystatechange = function () {
           console.log("INFO", xhr.status, xhr.responseText);
           if(xhr.status == 0){
               callback(false, "No internet connection or server error.");
           }
            if(xhr.status == 200){
                callback(true, JSON.parse(xhr.responseText)["message"]);
            }else{
                callback(false, JSON.parse(xhr.responseText)["message"]);
    }
    };

    xhr.send(); 
} 
