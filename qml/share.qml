import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "libs/qqr.js" as QRCodeBackend

Page{
id:qrPage
width: bottomEdge.width
height: bottomEdge.height
header: PageHeader {
id: qrheader
title: i18n.tr('Transfer')
}
Flickable{
    anchors{
        top:qrheader.bottom
        left:parent.left
        right:parent.right
        bottom:parent.bottom
    }
Column{
    spacing:1
    Label{
        id:dec
        width:qrPage.width
        text:i18n.tr("When scanned by another user, the item will be tranfered to this account.")
        wrapMode: Text.WordWrap
    }
    QRCode {
        id:codec
        width : Math.min(qrPage.width, qrPage.height-(qrheader.height+dec.height))
        height : Math.min(qrPage.width, qrPage.height-(qrheader.height+dec.height))
        value:root.qcode

    }
}
}
}
