import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "libs/accounts.js" as Acc
import Ubuntu.Components.Popups 1.3
import QtQuick.Window 2.2

Page {
    id: setPage
    header: PageHeader {
        id: setHeader
        title:i18n.tr("Settings");
    }
    property bool visiRE : sets.userNumRE != ""
    property bool visiACC : visiRE
    property string latestMessage : ""
    property bool ibanReCor : false
    //to avoid unneccessary Iban checks
    property string preIban : ""
    
    Timer {
    id: timer
    interval: 2000; repeat: true
    running: true
    triggeredOnStart: true
    onTriggered: {
        //Iban Checks
        if(ibanRE.text.length > 14){
            //for Relevo...
            //15 is minimal size (=norway)
            if(preIban != ibanRE.text){
                //only check if text changed to save energy
                console.log("checking iban...");
                preIban = ibanRE.text
                function bigMod(divident, divisor) {
                    //calculating modolu for the large number to check
                    var partLength = 14;

                    while (divident.length > partLength) {
                        var part = divident.substring(0, partLength);
                        divident = (part % divisor) +  divident.substring(partLength);          
                    }

                    return divident % divisor;
                }
                var check = ibanRE.text[2].toString()+ibanRE.text[3].toString()
                var cnum = (ibanRE.text[0].charCodeAt(0)-55).toString()+(ibanRE.text[1].charCodeAt(0)-55).toString()
                var val = bigMod((ibanRE.text.substring(4)+cnum.toString()+check.toString()), 97)
                if(val == 1){
                    ibanReCor = true;
                }else{
                    ibanReCor = false;
                }
            }
        }else{
            ibanReCor = false;
        }
    }
    }
    Component {
         id: fail
         Dialog {
             
             id: regDia
             title: i18n.tr("Failed")
             text: latestMessage
             Button {
                 text: i18n.tr("Close")
                 color: UbuntuColors.red
                 onClicked: PopupUtils.close(regDia)
             }
         }
    }
     Component {
         id: suc
         Dialog {
             
             id: regDia
             title: i18n.tr("Success")
             text: latestMessage
             Button {
                 text: i18n.tr("Close")
                 color: UbuntuColors.green
                 onClicked: PopupUtils.close(regDia)
             }
         }
    }
    Flickable{
        anchors {
            top: setHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            topMargin: units.gu(2)
         }
         Column{
            anchors.fill:parent
            spacing:units.gu(1)
            Label{
                id:accountsHead
                text:i18n.tr("Accounts")
                font.bold:true
                width:parent.width
                horizontalAlignment:Text.AlignHCenter
                font.pointSize: 25
                visible:visiACC
            }
            Label{
                text:i18n.tr("Not logged in yet")
                font.italic:true
                width:parent.width
                horizontalAlignment:Text.AlignHCenter
                font.pointSize: 20
                visible:!visiACC
            }
            Row{
                width:parent.width
                Text{
                    id:relevoHead
                    width:parent.width/2
                    text:"<hr><i>Relevo</i><hr>"
                    
                    //horizontalAlignment:Text.AlignHCenter
                    font.pointSize: 20
                    visible:visiRE
                }
                Text{
                    id:relevouserID1
                    width:parent.width/2
                    text:i18n.tr("User id:")+sets.userIDRE+" User number:"+sets.userNumRE
                    horizontalAlignment:Text.AlignRight
                    font.pointSize: 20
                    visible:visiRE && setPage.width > 900
                }
                Text{
                    id:relevouserID2
                    width:parent.width/2
                    text:i18n.tr("User id:")+sets.userIDRE+"<br>User number:"+sets.userNumRE
                    horizontalAlignment:Text.AlignRight
                    font.pointSize: 12
                    visible:visiRE && setPage.width  < 901
                }
            }
            Row{
                //anchors.fill:parent
                width:parent.width
                visible:visiRE
                //Options
                Button{
                    iconName:"delete"
                    text:i18n.tr("Delete Account")
                    color: UbuntuColors.red
                    width:parent.width/3
                }
                Button{
                    iconName:"system-log-out"
                    text:i18n.tr("Log out")
                    color: UbuntuColors.orange
                    width:parent.width/3
                    onClicked:{
                        console.log("id", sets.userIDRE, "token", sets.authKeyRE);
                        var informed = false;
                         Acc.logOutRe(sets.userNumRE, sets.authKeyRE, "de", function(status, data){
                            latestMessage = data["message"];
                            if(status && !informed){
                                //Logged off: inform and reset data.
                                PopupUtils.open(suc);
                                informed = true;
                                sets.userNumRE = "";
                                sets.userIDRE = "";
                                sets.authKeyRE = "";
                            }else{
                                if(!informed){
                                    PopupUtils.open(fail)
                                    informed = true;
                                }
                            }
                        });
                    }
                }
                Button{
                    id:editRE
                    iconName:"compose"
                    text:i18n.tr("Change")
                    width:parent.width/3
                    onClicked:{
                        if(editRE.iconName == "compose"){
                            namesRE.visible = true;
                            ibanRE.visible = true;
                            sepaConRE.visible = true
                            editRE.iconName = "go-up"
                        }else{
                            namesRE.visible = false;
                            ibanRE.visible = false;
                            sepaConRE.visible = false;
                            editRE.iconName = "compose";
                        }
                    }
                }
            }
            Row{
                width:parent.width
                id:namesRE
                visible:false
                TextField{
                    id:firstNameRE
                    width:parent.width/2
                    placeholderText: i18n.tr("First Name")
                }
                TextField{
                    id:lastNameRE
                    width:parent.width/2
                    placeholderText: i18n.tr("Last Name")
                }
            }
             Label{
                visible:ibanRE.text != "" && (firstNameRE.text == "" || lastNameRE.text == "")
                id:nameHint
                wrapMode: Text.Wrap
                width:parent.width
                color:UbuntuColors.red
                text:i18n.tr("Please fill out this fields to connect width SEPA.")
            }
            TextField{
                    id:ibanRE
                    visible:false
                    width:parent.width
                    placeholderText: i18n.tr("IBAN")
                }
            Label{
                visible:ibanRE.text != "" && !ibanReCor
                id:validHint
                color:UbuntuColors.red
                text:i18n.tr("IBAN is not valid.")
            }
            Button{
                id:sepaConRE
                visible:false
                width:parent.width
                color: UbuntuColors.green
                enabled:ibanRE.text != "" && ibanReCor && firstNameRE.text != "" && lastNameRE.text != ""
                text:"Add SEPA Connection"
                onClicked:{
                    //id, authkey, lang, iban, fName, lName, provider, callback
                    var informed = false;
                    Acc.sepaRe(sets.userNumRE, sets.authKeyRE, "de", ibanRE.text, firstNameRE.text, lastNameRE.text, function(status){
                            if(status && !informed){
                                //Because this endpoint does'nt provide api baised messages.
                                latestMessage = "Your account has been connected width the IBAN.";
                                //Logged off: inform and reset data.
                                PopupUtils.open(suc);
                                informed = true;
                            }else{
                                if(!informed){
                                    latestMessage = "The connection width Sepa has failed."
                                    PopupUtils.open(fail)
                                    informed = true;
                                }
                            }
                        });
                }
            }
            Label{
                id:reHints
                text:+i18n.tr("gethering Information...")
                width:parent.width
                wrapMode: Text.Wrap
                horizontalAlignment:Text.AlignHCenter
                font.pointSize: 10
                visible:visiACC
                Component.onCompleted:{
                    console.log("themeName", Theme.name, "pal", Theme.palette);
                    //gether account information from Api
                    Acc.infoRe(sets.userNumRE, sets.authKeyRE, "de", function(status, data){
                        var text = ""
                        if(!data["isConfigured"] || data["isTrial"]){
                            text = i18n.tr("Account is not completly configured. Please check if you confirmed your Mail-Adress, added a payment link and accepted the terms of service.<br>Otherwise your account might stop using after a few rentings.");
                        }else{
                            text = +i18n.tr("Account is copmpletly configured.<br>")
                        }
                        reHints.text = text;
                    });
                }
            }
            
         }
    }
}
