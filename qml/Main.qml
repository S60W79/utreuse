/*
 * Copyright (C) 2022  S60W79
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * reuse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "libs/lists.js" as List
MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'reuse.s60w79'
    automaticOrientation: true
    property var relevo : {}
    property string qcode : ''
    width: units.gu(45)
    height: units.gu(75)
    Settings{
        id:sets
        property string devicePrintRE : "eskaQy86TVK0GavSA6KDcB:APA91bFV5gS_FTND_8UjR4fBEtY1AtbUsHjZmfQBGGKBn4oKrKLRCZHLEYmjs1507moVpyGkEbBRXuIn0IVbjYxdBCv0WFMUxswXwMK6ckCUbpfiBhTqiPxgYNjP22y8NJPSWs6GfSH9"
        property string userIDRE : ""
        property string userNumRE : ""
        property string authKeyRE : ""
    }
AdaptivePageLayout {
    anchors.fill:parent
    primaryPage:pendingList
    Page {
        id:pendingList
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('UT REuse')
            trailingActionBar{ 
                
                numberOfSlots: 3
                actions : [
                    Action {
                        text : i18n.tr("Account")
                        iconName : "account"
                        onTriggered : {
                        pendingList.pageStack.addPageToNextColumn(pendingList, Qt.resolvedUrl("login.qml"));
                        }
                    },
                    Action {
                        text : i18n.tr("Settings")
                        iconName : "settings"
                        onTriggered : {
                        pendingList.pageStack.addPageToNextColumn(pendingList, Qt.resolvedUrl("settings.qml"));
                        }
                    },
                    Action {
                        text : i18n.tr("Locations")
                        iconName : "reload"
                        onTriggered : {
                            loader();
                        }
                    }
                    
                ]
            }
        }

        Label {
            id:standart
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            visible:false;
            text: i18n.tr('No Items rent')

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter
        }
        Rectangle {
            visible:false
            id:listArea
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
    width: parent.width; height: parent.height
    ListModel {
        id: itemsModel
       
    }
    Component {
        id: itemsDelegate
        Column{
            width:parent.width
            spacing:1
        
        ListItem {
            trailingActions: ListItemActions {
        actions: [
            Action {
                visible:extAble
                iconName: "timer"
                onTriggered:{
                    List.extend(sets.authKeyRE, "de", id, function(suc, response){
                        //extend response
                        console.log("extended:",response);
                        loader();
                    });
                    
                }
            },
            Action {
                iconName: "share"
                onTriggered:{
                    root.qcode = qr
                    pendingList.pageStack.addPageToNextColumn(pendingList, Qt.resolvedUrl("share.qml"))
                }
            }
        ]
    }
            width:parent.width
           // shall specify the height when Using ListItemLayout inside ListItem
           ListItemLayout {
               title.text: name
               subtitle.text: i18n.tr('Return in the next ')+lasting+i18n.tr(' days.')
           }
           onClicked:{
               //
               root.relevo = relevo;
               pendingList.pageStack.addPageToNextColumn(pendingList, Qt.resolvedUrl("relevoDet.qml"));
           }
           ProgressBar {
            width:parent.width
            id: duration
            minimumValue: 0.00
            maximumValue: 14.00
            value:14-lasting
            Label{
                id:qrID
                visible:false
                text:lstID
            }
               
        }
       }
        
        }
    }
    ListView {
        anchors.fill:parent
        model: itemsModel
        delegate: itemsDelegate
    }
}
BottomEdge {
            id: bottomEdge
            hint.text: i18n.tr("Swipe for new Item or returnal")
            height:parent.height
            hint.status:BottomEdgeHint.Active
            // override bottom edge sections to switch to real content
            contentUrl:Qt.resolvedUrl("scan.qml");
            BottomEdgeRegion {
                from: 0
                to:0.4
                contentUrl:Qt.resolvedUrl("scan2.qml")
            }
        }

    }

}
function loader(){
    //
    if(Theme.name.includes("Dark") || Theme.name.includes("dark")){
        //make Rectangle dark
        listArea.color=Qt.rgba(0.27, 0.27, 0.27);
    }
    itemsModel.clear();
    var informed = false
                            List.items(sets.authKeyRE, "de", function(status, rawList){
                                //
                                if(status && !informed){
                                    informed = true
                                    standart.visible=false
                                    listArea.visible=true
                                    rawList["items"].forEach(function(entry){
                                        //
                                        if(entry["itemStatus"] == "NOT_RETURNED"){
                                            itemsModel.append({"name":"Relevo "+entry["product"]["productName"], "lstID":entry["product"]["uId"], "lasting":Math.floor((entry["expiryTimeStamp"]-Date.now())/(60*60*24*1000)), "extAble":entry["canBeExtended"], "id":entry["id"], "qr":entry["product"]["uId"], "relevo":entry});
                                            console.log("lasting...", Math.floor((1672596799000-Date.now())/(60*60*24*1000)));
                                        }
                                    });
                                }
                            });
}
}
