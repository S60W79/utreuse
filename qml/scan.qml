import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Popups 1.3
import "libs/scanItem.js" as ScanItem
Page {
    width: bottomEdge.width
    height: bottomEdge.height
    header: PageHeader {
        id:scanHead
        title: i18n.tr("Add/Return Item")
    }
    Component {
         id: fail
         Dialog {
             
             id: regDia
             title: i18n.tr("Failed")
             text: latestMessage
             Button {
                 text: i18n.tr("Close")
                 color: UbuntuColors.red
                 onClicked: PopupUtils.close(regDia)
             }
         }
    }
     Component {
         id: suc
         Dialog {
             
             id: regDia
             title: i18n.tr("Success")
             text: latestMessage
             Button {
                 text: i18n.tr("Close")
                 color: UbuntuColors.green
                 onClicked: PopupUtils.close(regDia)
             }
         }
    }
    property string latestMessage : ""
    Flickable{
        anchors {
        top: scanHead.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        }
        width:parent.width
        Column{
            anchors.fill:parent
            spacing:1
        Label{
            width:parent.width
            id:disc
            text:i18n.tr("Here you can rent and return items.\nScan the QR-Code (on the box or the returning station) with Tagger or any similar QR-Code Reader. Then put the code in the line below.");
            font.italic:true
            wrapMode:Text.WordWrap
            horizontalAlignment:Text.AlignHCenter
        }
        Row{
        //     anchors {
        // top: disc.bottom
        // left: parent.left
        // right: parent.right
        // bottom: parent.bottom
        // }
            width:parent.width
        id:line
        TextField{
            id:code
            placeholderText:i18n.tr("Code")
            width:parent.width*0.9
        }
        Button{
                iconName:"tick"
                width:parent.width*0.1
                color:UbuntuColors.green
                onClicked:{
                    //trigger ScanItem
                    ScanItem.scan(sets.authKeyRE, sets.userNumRE, code.text, "de", function(success, response){
                        //executed
                        latestMessage = response;
                        if(success)PopupUtils.open(suc);
                        if(!success)PopupUtils.open(fail);
                    });
                }
            }
        }
        Button{
        //      anchors {
        // top: line.bottom
        // left: parent.left
        // right: parent.right
        // bottom: parent.bottom
        // }
        //width:parent.width
            width:parent.width
            text:"Insert from Clipboard"
            onClicked:code.text = Clipboard.data.text
            
            }
        }
    }
}
