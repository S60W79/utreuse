import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "libs/register.js" as Register
import Ubuntu.Components.Popups 1.3
// import Ubuntu.OnlineAccounts 2.0

Page {
    property string choosen : ""
    property bool isEqual:pwd.text==pwdRepeat.text
    property string latestMessage : ""
    id:loginPage
     Component {
         id: fail
         Dialog {
             
             id: regDia
             title: i18n.tr("Failed")
             text: latestMessage
             Button {
                 text: i18n.tr("Close")
                 color: UbuntuColors.red
                 onClicked: PopupUtils.close(regDia)
             }
         }
    }
     Component {
         id: suc
         Dialog {
             
             id: regDia
             title: i18n.tr("Success")
             text: latestMessage
             Button {
                 text: i18n.tr("Close")
                 color: UbuntuColors.green
                 onClicked: PopupUtils.close(regDia)
             }
         }
    }
    header: PageHeader {
        id: logHeader
        title:"Login/Register";
    }
    Flickable{
        anchors {
            top: logHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            topMargin: units.gu(2)
         }
        //Providers
        Column{
            anchors.fill:parent
            spacing:units.gu(1)
    
        ComboButton {
            id:provChoose
            text: "Choose Provider"
            width:parent.width
            Button{
                text:"Relevo"
                onClicked:{
                    provChoose.text="Relevo";
                    choosen = "Relevo";
                    provChoose.expanded = false;
                }

            }
        }
        TextField {
			id:userName
			width:parent.width
			placeholderText: i18n.tr("E-Mail Adress")
        }
        Row{

            
        Label{
            text:"Register a new Account"
        }
        Switch {
            id: regist
            checked:false;
        }
        }
        TextField {
			id:pwd
			width:parent.width
			placeholderText: i18n.tr("Password")
            echoMode: TextInput.Password
        }
        TextField {
			id:pwdRepeat
			width:parent.width
			visible:regist.checked
			placeholderText: i18n.tr("Repeat Password here")
            echoMode: TextInput.Password
        }
        Label{
            text:"Entries are not equal"
            visible:!isEqual && regist.checked
            color: UbuntuColors.red
        }
        Button{
            width:parent.width
            text:"Register"
            enabled:isEqual
            color: UbuntuColors.green
            visible:regist.checked
            onClicked:{
                //Register
                var informed = false;
                Register.relevoReg(userName.text, pwd.text, sets.devicePrintRE, "de", function(status, data){
                    latestMessage = data["message"];
                    if(status){
                        sets.userIDRE = data["user"]["userId"];
                        sets.authKeyRE = data["token"]
                        sets.userNumRE = data["user"]["id"];
                        if(!informed){
                            PopupUtils.open(suc);
                            informed = true;
                        }
                    }else{
                        if(!informed){
                            PopupUtils.open(fail)
                            informed = true;
                        }
                    }
                });
            }
        }
        Button{
            id:logBut
            width:parent.width
            text:"Log In"
            color: UbuntuColors.green
            visible:!regist.checked
            onClicked:{
                //Register
                var informed = false;
                Register.relevoLog(userName.text, pwd.text, sets.devicePrintRE, "de", function(status, data){
                    latestMessage = data["message"];
                    if(status){
                        sets.userIDRE = data["user"]["userId"];
                        sets.authKeyRE = data["token"]
                        sets.userNumRE = data["user"]["id"];
                        if(!informed){
                            PopupUtils.open(suc);
                            informed = true;
                        }
                    }else{
                        if(!informed){
                            PopupUtils.open(fail)
                            informed = true;
                        }
                    }
                });
            }
        }
        Button{
            text:"Account"
            onClicked:{
                //
                console.log(AccountModel.accountList);
            }
        }
//         ListView {
//     model: AccountModel {
//         //applicationId: "myapp.developer_myapp"
//     }
//     delegate: Text {
//         text: model.displayName
//     }
// }
}
    }
        
    }

