# UT REuse

Provides access to reusing/deposit systems for Ubuntutouch

**Warning: This has been tested, but not that long. Use at your own risk.**
The app will be published on the open store when some issues are fixed and more tests have been done.

## Implemented:

### Relevo

[Relevo](https://relevo.de/) is a reusing system, founded in Munich. You can borrow items by scanning codes depicted on the items. The staff will then have a look on your smartphone display and see the borrow confirmation. Returning works with returning qr codes at participating restaurants.

## Installation

* Install [Clickable](https://clickable-ut.dev/en/latest/)
* Download this repo e.g.: *git clone https://gitlab.com/S60W79/utreuse.git*
* connect your ubuntutouch device with your pc and run *clickable* on your pc
* for a trial on you desktop pc run *clickable desktop*


## License

Copyright (C) 2022  S60W79

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
